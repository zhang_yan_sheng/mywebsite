# mywebsite

#### 介绍

一个学习 Django 的简单工程。

#### 使用说明

1. 从仓库拉取代码

```
git clone https://gitee.com/zhang_yan_sheng/mywebsite.git
```

2. 执行 makemigrations

```
python manage.py makemigrations
```

3. 执行 migrate

```
python manage.py migrate
```

4. 创建 superuser

```
python manage.py createsuperuser
```

5. 运行
   运行之前请先配置好 Django 环境。

```
cd mywebsite
python manage.py runserver
```
